<?php
	/*Функция скачивания документов на главной странице*/
	function downloadMainDoc(){
		$doc = $_GET['doc'];
		$action = $_GET['action'];
		$json = file_get_contents("../docs/{$doc}/index.json");
		$data = json_decode($json, true);
		$count = count($data['versions']);
		for ($i=0; $i < $count; $i++) {
			$request = "{$data['versions'][$i]['FileName']}";
			$file = "../docs/{$doc}/{$data['versions'][$i]['FileName']}";
		}
		header('Content-Type: application/pdf');
		header("Content-Disposition: attachment; filename={$request}");
		readfile($file);
	}
	/*Функция просмотра всех возможных документов и определение расхождений данных документа с данными из index.json*/
	/*Если расхождений нет, то откывается alert с соответствующим текстом*/
	/*Если расхождения есть, то откывается страница с проблемными документами, можно скачать коррекные данные*/
	function checkWrongDoc(){
		$flag = true;
		$json = file_get_contents("../docs/index.json");
		$data = json_decode($json, true);
		$countFolder = count($data);
		$array = array();
		for ($i=0; $i < $countFolder; $i++) {
			$jsonDoc = file_get_contents("../docs/{$data[$i]}/index.json");
			$dataDoc = json_decode($jsonDoc, true);
			$countDoc = count($dataDoc['versions']);
			for ($j=0; $j < $countDoc; $j++) {
				$filename = "../docs/{$data[$i]}/{$dataDoc['versions'][$j]['FileName']}";
				$md5 = md5_file($filename);
				$size =filesize($filename);
				if ($md5 != $dataDoc['versions'][$j]['Md5'] || $size != $dataDoc['versions'][$j]['Size']) {
					$flag = false;
					if (!in_array($data[$i], $array)) {
						$array[] = $data[$i];
					}
				}
			}
		}
		if ($flag == true) {
			$message = "Ошибок и несоответствий в файлах не найдено";
			echo "<script type='text/javascript'>alert('{$message}');</script>";
		} else {
			$lengthArray = count($array);
			for ($i=0; $i < $lengthArray; $i++) {
				$request = $request . "document[]={$array[$i]}&";
			}
			header ("Location: ../web/index.php?{$request}");
		}
	}
	$doc = $_GET['doc'];
	$action = $_GET['action'];
	/*Условия выполнения скачивания документа на главной странице*/
	if(!isset($_GET['action']) && isset($_GET['doc']) && !isset($_GET['version']) && !isset($_GET['check'])) {
		downloadMainDoc();
	}
	/*Условия выполнения просмотра документов с расхожими данными*/
	if(($action == 'check') && !isset($_GET['doc'])) {
		checkWrongDoc();
	}
	/*Условия изменения отображаемого контента на странице*/
 	if($_SERVER['REQUEST_URI'] == '/web/index.php') { ?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="../assets/style.css">
		<title>Документы</title>
	</head>
	<body>
		<h1 class="title">Документы</h1>
		<div class="container">
		    <div class="col-sm">
		    	<div class="underTitle">
		    		<h2>Оферты</h2>
		    	</div>
				<div class="content">
					<table class="documents">
						<tr class="row">
							<td><a href="?doc=publOfferPau" class="docLink columnDoc">Публичная оферта ПАУ</a></td>
							<td><a href="?doc=publOfferPau&action=detail" class="docLink docFontSize columnDetail" target="_blank">(детали..)</a></td>
						</tr>
						<tr class="row">
							<td><a href="?doc=publOfferPauLimitedPeriod" class="docLink columnDoc">Публичная оферта ПАУ на ограниченный период</a></td>
							<td><a href="?doc=publOfferPauLimitedPeriod&action=detail" class="docLink docFontSize columnDetail" target="_blank">(детали..)</a></td>
						</tr>
						<tr class="row">
							<td><a href="?doc=publOfferPauTechSuport" class="docLink columnDoc">Публичная оферта на техподдержку</a></td>
							<td><a href="?doc=publOfferPauTechSuport&action=detail" class="docLink docFontSize columnDetail" target="_blank">(детали..)</a></td>
						</tr>
						<tr class="row">
							<td><a href="?doc=publOfferOnceUpd" class="docLink columnDoc">Публичная оферта на разовое обновление</a></td>
							<td><a href="?doc=publOfferOnceUpd&action=detail" class="docLink docFontSize columnDetail" target="_blank">(детали..)</a></td>
						</tr>
						<tr class="row">
							<td><a href="?doc=publOfferNewEquip" class="docLink columnDoc">Публичная оферта на новую комплектацию</a></td>
							<td><a href="?doc=publOfferNewEquip&action=detail" class="docLink docFontSize columnDetail" target="_blank">(детали..)</a></td>
						</tr>
						<tr class="row">
							<td><a href="?doc=publOfferChangePmAu" class="docLink columnDoc">Публичная оферта на замену РМ и АУ</a></td>
							<td><a href="?doc=publOfferChangePmAu&action=detail" class="docLink docFontSize columnDetail" target="_blank">(детали..)</a></td>
						</tr>
						<tr class="row">
							<td><a href="?doc=publOfferInfoServices" class="docLink columnDoc">Публичная оферта на оказание информационных услуг</a></td>
							<td><a href="?doc=publOfferInfoServices&action=detail" class="docLink docFontSize columnDetail" target="_blank">(детали..)</a></td>
						</tr>
						<tr class="row">
							<td><a href="?doc=publOfferShowcaseCreditorsCommittee" class="docLink columnDoc">Публичная оферта о комитете кредиторов на Витрине</a></td>
							<td><a href="?doc=publOfferShowcaseCreditorsCommittee&action=detail" class="docLink docFontSize columnDetail" target="_blank">(детали..)</a></td>
						</tr>
						<tr class="row">
							<td><a href="?doc=publOfferTradeDataShowcase" class="docLink columnDoc">Публичная оферта об обмене данными с Витриной</a></td>
							<td><a href="?doc=publOfferTradeDataShowcase&action=detail" class="docLink docFontSize columnDetail" target="_blank">(детали..)</a></td>
						</tr>
						<tr class="row">
							<td><a href="?doc=userAgreeWithBankroTECH" class="docLink columnDoc">Пользовательское соглашение об обмене данными с Bankro.TECH</a></td>
							<td><a href="?doc=userAgreeWithBankroTECH&action=detail" class="docLink docFontSize columnDetail" target="_blank">(детали..)</a></td>
						</tr>
					</table>
				</div>
		    </div>
		</div>
	</body>
	</html>
	<?php } elseif (isset($_GET['document'])) {
		require('../assets/errorFile.php');
	} else {
	require('../assets/document.php');
	}
?>
